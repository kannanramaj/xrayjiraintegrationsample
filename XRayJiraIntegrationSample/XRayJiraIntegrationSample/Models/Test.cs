﻿namespace XRayJiraIntegrationService.Models
{
    public class Test
    {
        public string testKey { get; set; }
        public DateTime start { get; set; }
        public DateTime finish { get; set; }
        public string comment { get; set; }
        public string status { get; set; }
    }
}
