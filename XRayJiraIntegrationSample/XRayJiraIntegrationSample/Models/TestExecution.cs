﻿namespace XRayJiraIntegrationService.Models
{
    public class TestExecution
    {
        public string testExecutionKey { get; set; }
        public Info info { get; set; }
        public List<Test> tests { get; set; }
    }
}
