﻿namespace XRayJiraIntegrationService.Models
{
    public class Info
    {
        public string summary { get; set; }
        public string description { get; set; }
        public string user { get; set; }
        public DateTime startDate { get; set; }
        public DateTime finishDate { get; set; }
        public List<string> testEnvironments { get; set; }
    }
}
