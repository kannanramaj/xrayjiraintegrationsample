﻿using System.Net;

namespace XRayJiraIntegrationService.Jira
{
    public class JiraRepository : IJiraRepository
    {
        public string GetDataFromJira(string url, string authentication)
        {
            try
            {
                using (var client = new WebClient())
                {
                    if (string.Compare("basic", authentication) == 0)
                    {
                        client.Headers.Add("Authorization", "Basic YXV0b21hdGlvbmNvZUBxdWFsaXRlc3Rncm91cC5jb206eHF1b0xiYXYwSENnRWo3NzJVUFIzNzk5");
                    }
                    else if ((string.Compare("bearer", authentication) == 0))
                    {
                        // update the token
                        client.Headers.Add("Authorization", "Bearer YXV0b21hdGlvbmNvZUBxdWFsaXRlc3Rncm91cC5jb206eHF1b0xiYXYwSENnRWo3NzJVUFIzNzk5");
                    }
                    var response = client.DownloadString(url);
                    return response;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception occured, {ex}");
                throw ex;
            }
        }
    }
}
