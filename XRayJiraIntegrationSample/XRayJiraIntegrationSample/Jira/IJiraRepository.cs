﻿namespace XRayJiraIntegrationService.Jira
{
    public interface IJiraRepository
    {
        string GetDataFromJira(string url, string authentication);
    }
}