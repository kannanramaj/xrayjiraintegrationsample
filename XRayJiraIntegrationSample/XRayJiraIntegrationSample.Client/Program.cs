﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using System.Text;
using XRayJiraIntegrationService.Jira;
using XRayJiraIntegrationService.Models;

Console.WriteLine("Hello, World!");

var issueIds = GetIssueIdsForTestKeys(new List<string> { "QFDV2-40"});

static List<int> GetIssueIdsForTestKeys(IEnumerable<string> testKeys)
{
    var issueIds = new List<int>();
    foreach (var testKey in testKeys)
    {
        var jiraRepository = new JiraRepository();
        var response = jiraRepository.GetDataFromJira($"https://qualitestgroupcoe.atlassian.net/rest/api/3/issue/{testKey}","basic");
        var xrayTest = JsonConvert.DeserializeObject<XRayTest>(response);
        issueIds.Add(xrayTest.id);
    }
    return issueIds;
}

static string CreateTestExecutionForIssueIds(List<int> issueIds)
{
    return "testExecutionKey";
}

static void UpdateTestStatusforTestKeys(Dictionary<string, string> testKeyResults)
{

}

static void AttachFileForTestKeys(Dictionary<string, string> testFileResults)
{

}
